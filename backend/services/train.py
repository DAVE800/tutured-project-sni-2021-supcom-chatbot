from pyexpat import model
from .model import *
from .prepro import *
import os

def train(lang,path,epoch,paths,model_path):
    prero=Prepro(path)
    prero.preprocess(lang,paths)
    x_train,y_train,training= prero.training(lang)
    print(training)
    model= Bot()
    model.train(x_train,y_train,len(training),len(prero.classes),epochs=epoch,path_name=model_path)

def multiple_language(epochs,model_path):
    prero=Prepro()
    x_train,y_train,train,labels=prero.prepro_all()
    model= Bot()
    model.train(x_train,y_train,len(train),len(labels),epochs,model_path)



  





