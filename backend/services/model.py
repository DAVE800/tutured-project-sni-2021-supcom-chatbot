from tensorflow.keras import layers
import tensorflow as tf
import matplotlib.pyplot as plt
plt.style.use('ggplot')
import numpy as np
import os
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout
from tensorflow.keras.optimizers import SGD

class Bot():
   
    def CNN_Model(self,input_dim,output_dim):
        max_features = 20000
        embedding_dim = input_dim+1
        inputs = tf.keras.Input(shape=(None,), dtype="int64")
        x = layers.Embedding(max_features, embedding_dim)(inputs)
        x = layers.Dropout(0.5)(x)
        x = layers.Conv1D(128, 7, padding="valid", activation="relu", strides=3)(x)
        x = layers.Conv1D(128, 7, padding="valid", activation="relu", strides=3)(x)
        x = layers.GlobalMaxPooling1D()(x)
        x = layers.Dense(128, activation="relu")(x)
        x = layers.Dropout(0.5)(x)
        predictions = layers.Dense(output_dim, activation="softmax", name="predictions")(x)
        model = tf.keras.Model(inputs, predictions) 
        
        return model
    def NN_Model(self,input_dim,output_dim):
        model = Sequential()
        model.add(Dense(128, input_shape=(input_dim,), activation='relu'))
        model.add(layers.Flatten())
        model.add(Dropout(0.5))
        model.add(Dense(64, activation='relu'))
        model.add(Dropout(0.5))
        model.add(Dense(output_dim, activation='softmax'))
        sgd = SGD(learning_rate=0.01, decay=1e-6, momentum=0.9, nesterov=True)
        model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=['accuracy'])
        return model
   
    def train(self,x,y,input_dim,output_dim,epochs,path_name):
       model = self.CNN_Model(input_dim,output_dim) 
       model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])
       hist=model.fit(x,y,epochs=epochs,batch_size=50,verbose=1)
       dir=os.path.basename="/home/dave/sni-bot/backend/models" 
       os.chdir(dir)
       model.save(path_name + '.h5', hist)

    


  