import numpy as np
import nltk
nltk.download('punkt')
nltk.download('wordnet')
from nltk.stem import WordNetLemmatizer
lemmatizer = WordNetLemmatizer()
import json
import pickle
import random
import os
nltk.download('stopwords')
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer

from .prepro import*
prep= Prepro()


def predict_class(msg,model,lang,words,classes):

        p = bow(msg, words,show_details=False,lang=lang)

        res = model.predict(np.array([p]))[0]

        ERROR_THRESHOLD = 0.25
        results = [[i,r] for i,r in enumerate(res) if r>ERROR_THRESHOLD]
        results.sort(key=lambda x: x[1], reverse=True)
        return_list = []
        for r in results:
            return_list.append({"intent": classes[r[0]], "probability": str(r[1])})
        return return_list
    
def getResponse(ints, intents_json,):
        tag = ints[0]['intent']
        list_of_intents = intents_json['intents']
        for i in list_of_intents:
            if(i['tag']== tag):
                result = random.choice(i['responses'])
                break
            else:
                continue

        return result


def chatbot_response(msg,model,intents,lang,words,classes):
        ints = predict_class(msg,model,lang,words,classes)
        res = getResponse(ints, intents)
        return res


def bow(sentence, words,show_details=True,lang=""):
        sentence_words = clean_up_sentence(sentence,lang)
        bag = [0]*len(words)
        for s in sentence_words:
            for i,w in enumerate(words):
                if w == s:
                    bag[i] = 1
                    if show_details:
                        print ("found in bag: %s" % w)
        return(np.array(bag))  


def clean_up_sentence(sentence,lang):
    setnc=prep.clean_up_text(language=lang,text=sentence)
    sn   =  SnowballStemmer(lang.lower())
    sentence_words = nltk.word_tokenize(setnc)
    if(lang=="english"):
        sentence_words = [lemmatizer.lemmatize(word.lower()) for word in sentence_words]
    if(lang=="french"):
        sentence_words = [sn.stem(word.lower()) for word in sentence_words]
    if(lang=="arabic"):
        sentence_words = [word.lower() for word in sentence_words]

    return sentence_words 

