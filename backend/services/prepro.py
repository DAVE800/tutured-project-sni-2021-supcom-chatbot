from cProfile import label
import re
import numpy as np
import nltk
nltk.download('punkt')
nltk.download('wordnet')
from nltk.stem import WordNetLemmatizer
lemmatizer = WordNetLemmatizer()
import json
import pickle
import random
import os
nltk.download('stopwords')
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer

class Prepro():
    def __init__(self,path=None) -> None:
        self.documents=None
        self.classes=None
        self.training_data=None 
        self.words=None
        self.labels=None
        if path:
            self.data=self.load_data(path)

    def clean_up_text(self,language,text):
        if(language.lower()=="english"):
            txt= re.sub("i'm","i am",text)
            txt= re.sub("you'er","you are",txt)
            txt = re.sub("she's","she is",txt,)
            txt = re.sub("['?',',','_'[0:9]']"," ",txt) 
            txt= re.sub("[',','.','?','9', '’']"," ",txt)
            return txt

        if(language.lower()=="french"):
            txt= re.sub("quelqu'un","quelque un",text)
            txt = re.sub("\?"," ",txt)
            txt = re.sub("c'est",' ',txt)
            txt = re.sub("c'est",'c est ',txt)
            txt = re.sub(",",' ',txt)
            txt= re.sub("allez-vous","allez vous",txt)
            txt = re.sub("d'hôpital","d hôpital",txt)
            txt=re.sub("donnez-moi","donnez moi",txt)
            txt= re.sub("-"," ",txt)
            txt= re.sub("'"," ",txt)
            txt = re.sub("['-',''']"," ",txt)
            return txt
       
        if(language.lower()!="english" or language.lower()!="french"):
            return text

    def word_stemming(self,language,save=True,path=None):
        stops = set(stopwords.words(language.lower()))
        sn   =  SnowballStemmer(language.lower())
        if(language.lower()!="arabic"):
            self.words = [sn.stem(w.lower()) for w in self.words if w not in stops]
        else:
            self.words = [lemmatizer.lemmatize(w.lower()) for w in self.words if w not in stops]

        self.words = sorted(list(set(self.words)))

        self.classes = sorted(list(set(self.classes)))
        
        if(save):
            dir ="/home/dave/sni-bot/backend/vocabs"
            os.chdir(dir)
            pickle.dump(self.words,open(path["words"] +'.pkl','wb'))

            dir ="/home/dave/sni-bot/backend/classes"
            os.chdir(dir) 

            pickle.dump(self.classes,open(path["classes"]+'.pkl','wb'))



    def load_data(self,path):
        with open(path,mode="r") as data:
            return json.loads(data.read()) 


    def training(self,lang):
        training = []
        if(lang.lower()=="arabic"):
            sn=lemmatizer
        else:
            sn   =  SnowballStemmer(lang.lower())

        output_empty = [0] * len(self.classes)
        for doc in self.documents:

            bag = []
            pattern_words = doc[0]
            if(lang.lower()=="arabic"):
                pattern_words = [sn.lemmatize(word.lower()) for word in pattern_words]
            else:
                pattern_words = [sn.stem(word.lower()) for word in pattern_words]

            for w in self.words:
                bag.append(1) if w in pattern_words else bag.append(0)

            output_row = list(output_empty)
            output_row[self.classes.index(doc[1])] = 1

            training.append([bag, output_row])
        random.shuffle(training)
        training_data=training
        training = np.array(training)
        train_x = list(training[:,0])
        train_y = list(training[:,1]) 
        return (train_x,train_y,training_data)

    def preprocess(self,lang,path):
        classes= []
        words= []
        documents=[]
        intents= self.data["intents"]
        for intent in intents:
            for pattern in intent['patterns']:
                p=self.clean_up_text(lang,pattern) 
                w = nltk.word_tokenize(p)
                words.extend(w)
                documents.append((w, intent['tag']))

            if intent['tag'] not in classes:
                classes.append(intent['tag']) 
        self.documents=documents
        self.classes=classes
        self.words=words
        self.word_stemming(language=lang,path=path)

    def clean_fr_text(self,txt):

        text= re.sub("quelqu'un","quelque un",txt)
        text = re.sub("\?"," ",text)
        text = re.sub("c'est",' ',text)
        text = re.sub("c'est",'c est ',text)
        text = re.sub(",",' ',text)
        text= re.sub("allez-vous","allez vous",text)
        text = re.sub("d'hôpital","d hôpital",text)
        text=re.sub("donnez-moi","donnez moi",text)
        text= re.sub("-"," ",text)
        text= re.sub("'"," ",text)
        return text

            
    def clean_en_text(self,text):
        txt= re.sub("i'am","i am",text)
        txt = re.sub("you're","you are",txt)
        txt= re.sub("'"," ",txt)
        txt= re.sub("[',','.','?','9', '’']"," ",txt)

        return  txt

    def prepro_all(self):
            with open("/home/dave/sni-bot/backend/services/intents/intents_ar.json",mode="r") as arabic_df:
                    arabic_df=json.loads(arabic_df.read())

            with open("/home/dave/sni-bot/backend/services/intents/intents_en.json",mode="r") as english_df:
                    english_df=json.loads(english_df.read())


            with open("/home/dave/sni-bot/backend/services/intents/intents_fr.json",mode="r") as french_df:
                    french_df=json.loads(french_df.read())
            
            arabic_intents = arabic_df["intents"]
            english_intents= english_df["intents"]
            french_intents=french_df["intents"]
            arabic_words=[]
            arabic_classes = []
            arabic_documents = []
            stops = set(stopwords.words('arabic'))
            for intent in arabic_intents:
                for pattern in intent['patterns']:
                    # we take each word and tokenize it
                    w = nltk.word_tokenize(pattern)
                    arabic_words.extend(w)
                    # we it add documents
                    arabic_documents.append((w, intent['tag']))

                    # we add it  to classes to our class list

                    if intent['tag'] not in arabic_classes:
                        arabic_classes.append(intent['tag'])
            arbic_words = [lemmatizer.lemmatize(w.lower()) for w in arabic_words if w not in stops]

            arabic_words = sorted(list(set(arabic_words)))

            arabic_classes = sorted(list(set(arabic_classes)))

            english_words=[]
            english_classes = []
            english_documents = []
            stops = set(stopwords.words('english'))

            for intent in english_intents:
                for pattern in intent['patterns']:
                #let clea each text in the patterns
                    p = self.clean_en_text(pattern)
                    # we take each word and tokenize it
                    w = nltk.word_tokenize(p)
                    english_words.extend(w)
                    # we it add documents
                    english_documents.append((w, intent['tag']))

                    # we add it  to classes to our class list

                    if intent['tag'] not in english_classes:
                        english_classes.append(intent['tag'])
            
            english_words = [lemmatizer.lemmatize(w.lower()) for w in english_words if w not in stops]

            english_words = sorted(list(set(english_words)))

            english_classes = sorted(list(set(english_classes)))

            french_words=[]
            french_classes = []
            french_documents = []
            stops = set(stopwords.words('french'))
            for intent in french_intents:
                for pattern in intent['patterns']:
                #let clea each text in the patterns
                    p = self.clean_fr_text(pattern)
                    # we take each word and tokenize it
                    w = nltk.word_tokenize(p)
                    french_words.extend(w)
                    # we it add documents
                    french_documents.append((w, intent['tag']))

                    # we add it  to classes to our class list

                    if intent['tag'] not in french_classes:
                        french_classes.append(intent['tag'])
            sn   =  SnowballStemmer("french")
            french_words = [sn.stem(w.lower()) for w in french_words if w not in stops]

            french_words = sorted(list(set(french_words)))

            french_classes = sorted(list(set(french_classes)))

            self.labels=french_classes+english_classes+arabic_classes
            self.words= french_words+english_words+arabic_words

            documents= []
            for i in french_documents:
                documents.append(i)
            for i in english_documents:
                documents.append(i)
            for i in arabic_documents:
                documents.append(i)
            training = []
            output_empty = [0] * len(self.labels)
            for doc in documents:
                bag = []
                pattern_words = doc[0]
                pattern_words = [lemmatizer.lemmatize(word.lower()) for word in pattern_words]
                for w in self.words:
                    bag.append(1) if w in pattern_words else bag.append(0)

                output_row = list(output_empty)
                output_row[self.labels.index(doc[1])] = 1

                training.append([bag, output_row])
            random.shuffle(training)
            training = np.array(training)
            train_x = list(training[:,0])
            train_y = list(training[:,1])
    
            dir ="/home/dave/sni-bot/backend/classes/"
            os.chdir(dir) 
            pickle.dump(self.classes,open(os.path.join(dir,'labels.pkl'),'wb'))
            dir ="/home/dave/sni-bot/backend/vocabs/"
            os.chdir(dir) 
            pickle.dump(self.classes,open(os.path.join(dir,'vocabs.pkl'),'wb'))
            
            return (train_x,train_y,training,self.labels)

                                                

                        
                        
                                    


    
