from crypt import methods
from keras.models import load_model
from services.helpers import*
from flask import Flask
from flask_restful import Resource, Api
from flask import Flask, request
from language_detector import detect_language
from flask.json import jsonify
from flask_cors import CORS
from flasgger import Swagger
from langdetect import detect

import pickle
import json
modeldir="/home/dave/sni-bot/backend/models/"
wordsdir="/home/dave/sni-bot/backend/vocabs/"
classesdir="/home/dave/sni-bot/backend/classes/"
intentsdir="/home/dave/sni-bot/backend/services/intents/"
app = Flask(__name__)

cors = CORS(app, resources={r"/sni/*": {"origins": "*"}})

api = Api(app)
Swagger(app)

@app.route('/sni/chatbot', methods=['POST'])
def index(message='user message'):
    """
    SNI Chatbot API Natural Processing Understanding 
    ---
    tags:
      - Chat in Arabic, French and English
    parameters:
      - name: body
        in: body
        schema:
          id: demand
          required:
            - message
          properties:
             message:
                type: string
                description: a wonderfull multiple language chatbot for medical support
                default: 'hello'
    responses:
      200:
        description: The required result is available
      500:
        description: Error!
    """
    jsonObj = request.get_json()
    demand = jsonObj.get('message')
    print(detect_language(demand))
    if(detect_language(demand)=="English"):
            model = load_model(os.path.join(modeldir,'english.h5'))
            intents = json.loads(open(os.path.join(intentsdir,'intents_en.json')).read())
            words = pickle.load(open(os.path.join(wordsdir,'english_words.pkl'),'rb'))
            classes = pickle.load(open(os.path.join(classesdir,'english_classes.pkl'),'rb'))
            r=chatbot_response(msg=demand,model=model,intents=intents,lang="english",words=words,classes=classes)
            print(r)
            x='{"response":"'+r+'"}'
            print(x)
            return x
    elif(detect_language(demand)=="French"):
            model = load_model(os.path.join(modeldir,'french.h5'))
            intents = json.loads(open(os.path.join(intentsdir,'intents_fr.json')).read())
            words = pickle.load(open(os.path.join(wordsdir,'french_words.pkl'),'rb'))
            classes = pickle.load(open(os.path.join(classesdir,'french_classes.pkl'),'rb'))
            r=chatbot_response(msg=demand,model=model,intents=intents,lang="french",words=words,classes=classes)
            print(r)
            x='{"response":"'+r+'"}'
            print(x)
            return x
    elif (detect_language(demand)=="Arabic"):
            model = load_model(os.path.join(modeldir,'arabic.h5'))
            intents = json.loads(open(os.path.join(intentsdir,'intents_ar.json')).read())
            words =   pickle.load(open(os.path.join(wordsdir,'arabic_words.pkl'),'rb'))
            classes = pickle.load(open(os.path.join(classesdir,'arabic_classes.pkl'),'rb'))
            r=chatbot_response(msg=demand,model=model,intents=intents,lang="arabic",words=words,classes=classes)
            print(r)
            x='{"response":"'+r+'"}'
            print(x)
            return x
    else:
        r="sorry i dont understand that language"
        x='{"response":"'+r+'"}'
        return x

@app.route('/sni/french/chatbot', methods=['POST'])
def french():
    jsonObj = request.get_json()
    demand = jsonObj.get('message')
    model = load_model(os.path.join(modeldir,'french.h5'))
    intents = json.loads(open(os.path.join(intentsdir,'intents_fr.json')).read())
    words = pickle.load(open(os.path.join(wordsdir,'french_words.pkl'),'rb'))
    classes = pickle.load(open(os.path.join(classesdir,'french_classes.pkl'),'rb'))
    r=chatbot_response(msg=demand,model=model,intents=intents,lang="french",words=words,classes=classes)
    print(r)
    x='{"response":"'+r+'"}'
    print(x)
    return x

@app.route('/sni/english/chatbot', methods=['POST'])
def english():
    jsonObj = request.get_json()
    demand = jsonObj.get('message')
    model = load_model(os.path.join(modeldir,'english.h5'))
    intents = json.loads(open(os.path.join(intentsdir,'intents_en.json')).read())
    words = pickle.load(open(os.path.join(wordsdir,'english_words.pkl'),'rb'))
    classes = pickle.load(open(os.path.join(classesdir,'english_classes.pkl'),'rb'))
    r=chatbot_response(msg=demand,model=model,intents=intents,lang="english",words=words,classes=classes)
    print(r)
    x='{"response":"'+r+'"}'
    print(x)
    return x

@app.route('/sni/arabic/chatbot', methods=['POST'])
def arabic():
    jsonObj = request.get_json()
    demand = jsonObj.get('message')
    model = load_model(os.path.join(modeldir,'arabic.h5'))
    intents = json.loads(open(os.path.join(intentsdir,'intents_ar.json')).read())
    words =   pickle.load(open(os.path.join(wordsdir,'arabic_words.pkl'),'rb'))
    classes = pickle.load(open(os.path.join(classesdir,'arabic_classes.pkl'),'rb'))
    r=chatbot_response(msg=demand,model=model,intents=intents,lang="arabic",words=words,classes=classes)
    print(r)
    x='{"response":"'+r+'"}'
    print(x)
    return x

@app.route('/sni/chatbot',methods=["POST"])
def multiplelanguage():
    jsonObj = request.get_json()
    demand = jsonObj.get('message')
    model = load_model(os.path.join(modeldir,'sni.h5'))
    intents = json.loads(open(os.path.join(intentsdir,'intents.json')).read())
    words =   pickle.load(open(os.path.join(wordsdir,'vocabs.pkl'),'rb'))
    classes = pickle.load(open(os.path.join(classesdir,'labels.pkl'),'rb'))
    r=chatbot_response(msg=demand,model=model,intents=intents,lang="arabic",words=words,classes=classes)
    print(r)
    x='{"response":"'+r+'"}'
    print(x)
    return x




if __name__=="__main__":
        app.run(debug=True)

